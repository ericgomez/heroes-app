import { useState } from 'react'

export const useForm = (initState = {}) => {
  const [state, setState] = useState(initState)

  const reset = () => {
    setState(initState)
  }

  const handleInputChange = ({ target }) => {
    setState({
      ...state,
      [target.name]: target.value
    })
  }

  return [state, handleInputChange, reset]
}
