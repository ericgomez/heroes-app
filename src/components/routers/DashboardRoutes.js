import React from 'react'
import { Routes, Route } from 'react-router-dom'

import { Navbar } from '../ui/Navbar'
import { MarvelScreen } from '../marvel/MarvelScreen'
import { HeroScreen } from '../heroes/HeroScreen'
import { DcScreen } from '../dc/DcScreen'
import { SearchScreen } from '../search/SearchScreen'

export const DashboardRoutes = () => {
  return (
    <>
      <Navbar />
      <div className='container mt-2'>
        <Routes>
          {/* <Route path="/" element={<Home />} /> */}
          <Route exact path='marvel' element={<MarvelScreen />} />
          <Route exact path='hero/:heroId' element={<HeroScreen />} />
          <Route exact path='dc' element={<DcScreen />} />
          <Route exact path='search' element={<SearchScreen />} />

          <Route path='*' element={<MarvelScreen />} />
        </Routes>
      </div>
    </>
  )
}
