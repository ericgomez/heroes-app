import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'


import { LoginScreen } from '../login/LoginScreen'
import { DashboardRoutes } from './DashboardRoutes';

export const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route exact path='/login' element={<LoginScreen />} />
        <Route exact path='/*' element={<DashboardRoutes />} />
      </Routes>
    </Router>
  )
}
