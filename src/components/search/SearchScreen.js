import React, { useMemo } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import queryString from 'query-string'

import { useForm } from '../../hooks/useForm'
import { HeroCard } from '../heroes/HeroCard'
import { getHeroesByName } from '../../selectors/getHeroesByName'

export const SearchScreen = () => {
  let navigate = useNavigate()
  const { search } = useLocation()

  // queryString to get the search query
  const { q = '' } = queryString.parse(search)
  // console.log(q)

  const [state, handleInputChange] = useForm({
    hero: q
  })

  const { hero } = state

  // get the filtered heroes
  // useMemo is used to avoid re-rendering the component when the state changes (only when the q changes)
  const heroesFiltered = useMemo(() => getHeroesByName(q), [q])

  const handleSearch = e => {
    e.preventDefault()
    // get the value of the input and pass it to the queryString
    navigate(`?q=${hero}`)
  }

  return (
    <div className='row'>
      <h1>Search Screen</h1>
      <hr />
      <div className='col-5'>
        <h4>Search Form</h4>
        <hr />

        <form onSubmit={handleSearch}>
          <input
            type='text'
            placeholder='Find your hero'
            className='form-control'
            autoComplete='off' // para que no se autocomplete
            name='hero'
            value={hero}
            onChange={handleInputChange}
          />
          <button
            type='submit'
            className='btn m-1 btn-block btn-outline-primary'
          >
            Search...
          </button>
        </form>
      </div>
      <div className='col-7'>
        <h4>Search Results</h4>
        <hr />

        {/* if the q is empty, show a message */}
        {q === '' && <div className='alert alert-info'>Search a hero</div>}

        {/* if the q is empty, show a message */}
        {q !== '' && heroesFiltered.length === 0 && (
          <div className='alert alert-danger'>There is no a hero with {q}</div>
        )}

        {heroesFiltered.map(hero => (
          <HeroCard key={hero.id} {...hero} />
        ))}
      </div>
    </div>
  )
}
