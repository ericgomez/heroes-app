import React, { useMemo } from 'react'
import { Navigate, useParams, useNavigate } from 'react-router-dom'
import { getHeroById } from '../../selectors/getHeroById'

export const HeroScreen = () => {
  let navigate = useNavigate()
  // get the hero id from the url
  const { heroId } = useParams()

  const hero = useMemo(() => getHeroById(heroId), [heroId])

  if (!hero) {
    // if the hero is not found, redirect to the home page
    return <Navigate to='/' replace />
  }

  const handleReturn = () => {
    // navigate to the heroes list go 1 page back
    navigate(-1)
  }

  const { superhero, alter_ego, publisher, first_appearance, characters } = hero

  return (
    <div className='row mt-5'>
      <div className='col-4'>
        <img
          src={`../assets/heroes/${heroId}.jpg`}
          className='img-thumbnail animate__animated animate__fadeInLeft'
          alt={superhero}
        />
      </div>
      <div className='col-8'>
        <h3>{superhero}</h3>
        <ul className='list-group list-group-flush'>
          <li className='list-group-item'>Alter ego: {alter_ego}</li>
          <li className='list-group-item'>Publisher: {publisher}</li>
          <li className='list-group-item'>
            First appearance: {first_appearance}
          </li>
        </ul>

        <h5>Characters</h5>
        <p>{characters}</p>

        <button className='btn btn-outline-info' onClick={handleReturn}>
          Return
        </button>
      </div>
    </div>
  )
}
