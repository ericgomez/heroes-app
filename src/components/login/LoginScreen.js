import React from 'react'
import { useNavigate } from 'react-router-dom'

export const LoginScreen = () => {
  let navigate = useNavigate()

  const handleLogin = () => {
    // navigate('/')
    navigate('/', { replace: true }) // replace: eliminar la historia de navegación
  }

  return (
    <div className='container mt-5'>
      <h1>Login Screen</h1>
      <hr />

      <button className='btn btn-primary' onClick={handleLogin}>
        Login
      </button>
    </div>
  )
}
