import React from 'react'
import { Link, NavLink } from 'react-router-dom'

export const Navbar = () => {
  return (
    <>
      <nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
        <Link className='navbar-brand' to='/'>
          Asociaciones
        </Link>

        <div className='navbar-collapse'>
          <div className='navbar-nav'>
            <NavLink
              className={({ isActive }) =>
                'nav-item nav-link' + (isActive ? ' activated' : '')
              }
              to='/marvel'
            >
              Marvel
            </NavLink>

            <NavLink
              className={({ isActive }) =>
                'nav-item nav-link' + (isActive ? ' activated' : '')
              }
              to='/dc'
            >
              DC
            </NavLink>

            <NavLink
              className={({ isActive }) =>
                'nav-item nav-link' + (isActive ? ' activated' : '')
              }
              to='/search'
            >
              Search
            </NavLink>
          </div>
        </div>

        <div className='order-3 dual-collapse2'>
          <ul className='navbar-nav ml-auto'>
            <NavLink
              className={({ isActive }) =>
                'nav-item nav-link' + (isActive ? ' activated' : '')
              }
              to='/login'
            >
              Logout
            </NavLink>
          </ul>
        </div>
      </nav>
      {/* Usaremos un <Outlet> donde solían estar las rutas para representar las rutas secundarias. */}
      {/* <Outlet /> */}
    </>
  )
}
