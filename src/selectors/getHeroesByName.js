import { heroes } from '../data/heroes'

export const getHeroesByName = name => {
  // if (name === '') return array empty
  if (name === '') {
    return []
  }

  return heroes.filter(hero =>
    // filter by name and return the hero
    hero.superhero.toLocaleLowerCase().includes(name.toLocaleLowerCase())
  )
}
